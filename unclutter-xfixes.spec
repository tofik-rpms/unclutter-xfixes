%if 0%{?fedora} > 0 && 0%{?fedora} <= 35
%global debug_package %{nil}
%endif

Name:           unclutter-xfixes
Version:        1.6
Release:        2%{?dist}
Summary:        A tool to hide the mouse cursor when not used. It might be useful when you use your keyboard, especially in tilig window managers.

License:        MIT
URL:            https://github.com/Airblader/unclutter-xfixes
Source0:        https://github.com/Airblader/unclutter-xfixes/archive/refs/tags/v%{version}/%{name}-%{version}.tar.gz 

Provides:       unclutter

BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  asciidoc
BuildRequires:  libev-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libX11-devel
BuildRequires:  libXi-devel

%description
This is a rewrite of the popular tool unclutter, but using the x11-xfixes extension. This means that this rewrite doesn't use fake windows or pointer grabbing and hence causes less problems with window managers and/or applications.


%prep
%autosetup


%build
%make_build


%install
%make_install
rm -fr %{buildroot}%{_defaultlicensedir}/unclutter


%check


%files
%license LICENSE
%{_mandir}/man1/*
%{_bindir}/*

%changelog
* Tue Mar 08 2022 Jerzy Drożdż - <jerzy.drozdz@jdsieci.pl> - 1.6-2
- Added Provides clause

* Tue Mar 08 2022 Jerzy Drożdż - <jerzy.drozdz@jdsieci.pl> - 1.6-1
- Initial build
